const controllers = require('./controller.js');
const schema= require('./schema.js');
const router = require('express').Router();
const userAuth= require('../middlewares/usersAuth');

router.get('/', userAuth.userAuth,controllers.getUsers);
router.get('/:id',userAuth.userAuthId,schema.getUser ,controllers.getUser);
router.post('/',userAuth.userAuthPost ,schema.createUser,controllers.createUser);
router.put('/:id', userAuth.userAuthId,schema.updateUser,controllers.updateUser);
router.delete('/:id',userAuth.userAuthId,schema.getUser ,controllers.deleteUser);

module.exports = router;
