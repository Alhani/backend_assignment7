const joi = require('joi');

const schemaBody = joi.object(
    {
        first_name: joi.string().max(50).required(),
        last_name: joi.string().max(50),
        age: joi.number().min(8),
        email: joi.string().max(50).email().required(),
        password: joi.string().max(50).required(),
    }
)

const createUser = async (req, res, next) => {
    try {
        const { error, value } =  schemaBody.validate(req.body);
       
        if (error) {

            
            res.json({Error:error.details[0].message})
            console.log(error);
            

        }
        else {
            next()
            console.log('User successfully created');
            
        }

    } catch (error) {
         next(error);
    }
       


   
};

const schemaId = joi.object(
    {
        id: joi.number().min(1).integer().positive().required(),
    }
);
const getUser = async (req, res, next) => {
    try {
        const { error, value } =  schemaId.validate(req.params);
        if (error) {
            
            res.json({Error:error.details[0].message});
        }
        else {
            next();
        }
    } 
    catch (error) {
         next(error);
    }
};

const updateUser = async (req, res, next) => {
    try {
        const { errorId, valueId } =  schemaId.validate(req.params);
        const { error, value } =  schemaBody.validate(req.body);
        if (errorId) {
            res.json({Error:errorId.message})
            console.log(errorId.message);
        }
        else if (error) {
            res.json({Error:error.details[0].message})
            console.log(error);
        }
        else {
            next();
        }

    }
    catch (error) {
         next(error);
         
        

    }
};

module.exports = {
    createUser,
    getUser,
    updateUser,
}
