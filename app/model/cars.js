const Database = require('../core/database');

const getCars = async () => {
  const query = `
    select * from cars.cars;
  `;
  const [rows, fields] = await Database.query(query);
  return rows;
}





const getCar = async (carId) => {
  const query = `
    select * from cars.cars where id = ?;
  `;
  const [rows, fields] = await Database.query(query, [carId]);
  return rows[0];
}


const createCar = async (series, company, color, owner_id) => {
  const query = `
    INSERT INTO cars.cars (series, company, color, owner_id) VALUES (?, ?, ?, ?);
  `;
  const params = [series, company, color, owner_id];
  const [result] = await Database.query(query, params);
  return result;
}

const updateCar = async (carId,series, company, color, owner_id) => {
  const updateData = {
    series,
    company, 
    color, 
    owner_id
  };
  
  const params = [];
  const updateStatements = [];

  
  for (const [key, value] of Object.entries(updateData)) {
    if (value !== undefined) {
      updateStatements.push(`${"`" + key + "`"} = ?`);
      params.push(value);
    }
  }
  const query = `
    UPDATE cars.cars SET ${updateStatements.join(',')} WHERE id = ?;
  `;
  params.push(carId);
  const [result] = await Database.query(query, params);
  return result;
}

const deleteCar= async (carId) => {
  const query = `
    delete from cars.cars where id = ?;
  `;
  const [result] = await Database.query(query, [carId]);
  return result;
}

// add query for car authorization 
const carAuthPost= async (password,id)=>{
  const query = `
  select * from users where password=? and id=?;
`;
const [result] = await Database.query(query, [password,id]);
return result;
};

const carAuthUpdate=async(password,id)=>{
  const query = `
  select * from users where password=? and id=?;
`;
const [result] = await Database.query(query, [password,id]);
return result
}

const carAuthId =async(password,id)=>{
  const query = `
  select *  from cars join users u on u.id = cars.owner_id where password = ? and cars.id =?;
  `;
  const [result] = await Database.query(query, [password,id]);
return result
}

const carsAuthGet =async (password)=>{
  const query = `
  select *  from cars join users u on u.id = cars.owner_id where password = ? ;
  `;
  const [result] = await Database.query(query, [password]);
return result
}
module.exports = {
  getCars,
  getCar,
  createCar,
  updateCar,
  deleteCar,
  carAuthPost,
  carAuthUpdate,
  carAuthId,
  carsAuthGet,
};
