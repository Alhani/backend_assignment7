const express = require('express');
require('dotenv').config();
const monitoring=require('./middlewares/monitoring')
const errorHandler = require('../app/middlewares/errorHandler');
const UserRoutes = require('./users/routes');
const CarRoutes=require('./cars/routes');


const app = express();

app.use(express.json());
app.use(monitoring);
app.get('/', (req, res) => {
  res.send('Server is running!');
});

app.use('/api/users', UserRoutes);
app.use('/api/cars', CarRoutes);
app.use(errorHandler)
module.exports = app;