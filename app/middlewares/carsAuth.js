const Cars = require('../model/cars');

const carAuthPost=async(req,res,next)=>{
    const id=req.body.owner_id;
    const password=req.headers.authorization;
    try {
        const result= await Cars.carAuthPost(password,id);
        if(result.length !==0){
            next();
        }
        else {
            res.status(401).json({ massage: 'Not Authorization, try again' });
        }
    } catch (error) {
        next(error)
    }

};


const carAuthUpdate = async (req,res,next)=>{
    const id=req.body.owner_id;
    const password=req.headers.authorization;
    try {
        const result= await Cars.carAuthPost(password,id);
        if(result.length !==0){
            next();
        }
        else {
            res.status(401).json({ massage: 'Not Authorization, try again' });
        }
    } catch (error) {
        next(error)
    }

}


const carAuthId =async(req,res,next)=>{
    const id=req.params.id;
    const password=req.headers.authorization;
    try {
        const result = await Cars.carAuthId(password,id);
        if(result.length !==0){
            next();
        }
        else {
            res.status(401).json({ massage: 'Not Authorization, try again' });
        }
        
    } catch (error) {
        next(error)
    }

}
const carsAuthGet =async(req,res,next)=>{
    const password=req.headers.authorization;
    try {
        const result = await Cars.carsAuthGet(password);
        if(result.length !==0){
            next();
        }
        else {
            res.status(401).json({ massage: 'Not Authorization, try again' });
        }
        
    } catch (error) {
        next(error)
    }
}


module.exports={
    carAuthPost,
    carAuthUpdate,
    carAuthId,
    carsAuthGet,
}