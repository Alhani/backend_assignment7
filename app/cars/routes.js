const controllers = require('./controller');
const schema= require('../cars/scheme');
const router = require('express').Router();
const carAuth= require('../middlewares/carsAuth')

router.get('/',carAuth.carsAuthGet,controllers.getCars);
router.get('/:id' ,carAuth.carAuthId,carAuth.carAuthId,schema.getCar,controllers.getCar);
router.post('/',carAuth.carAuthPost,schema.createCar,controllers.createCar);
router.put('/:id',carAuth.carAuthUpdate,schema.updateCar,controllers.updateCar);
router.delete('/:id',carAuth.carAuthId,schema.getCar,controllers.deleteCar);

module.exports = router;
