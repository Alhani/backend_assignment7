const Cars = require('../model/cars');

const getCars = async (req, res, next) => {
  try {
    const cars = await Cars.getCars();
    res.json(cars);
  } catch (error) {

     next(error);
  }
}

const getCar = async (req, res, next) => {
  try {
    const carId = req.params.id;
    const cars = await Cars.getCar(carId);
    if (!cars) {
      res.status(404).json({ message: "Car not found!" });
    } else {
      res.json(cars);
    }
  } catch (error) {
   
     next(error);
  }
}

const createCar = async (req, res, next) => {
  try {
    const { series, company, color, owner_id } = req.body;
    const insertionResult = await Cars.createCar(series, company, color, owner_id);
    res.status(201).json({
      message: "car successfully created.",
      insertedId: insertionResult.insertId
    });
  } catch (error) {

     next(error);
  }
}

const updateCar = async (req, res, next) => {
  try {
    const carId = req.params.id;
    const {series, company, color, owner_id } = req.body;
    await Cars.updateCar(carId, series, company, color, owner_id);
    res.json({
      message: "car successfully updated."
    });
  } catch (error) {
     next(error);

  }
}

const deleteCar = async (req, res, next) => {
  try {
    const carId = req.params.id;
    console.log('carId :>> ', carId);
    const deleteResult = await Cars.deleteCar(carId);
    if (!deleteResult.affectedRows) {
      res.status(404).json({ message: "car not exists!" });
    } else {
      res.json({ message: "car successfully deleted." });
    }
  } catch (error) {

    next(error);
  }
}




module.exports = {
  getCars,
  getCar,
  createCar,
  updateCar,
  deleteCar,
};
