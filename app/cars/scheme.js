const joi = require('joi');

const schemaBody = joi.object(
    {
      
        series: joi.string().max(50).required(),
        company: joi.string().max(50).required(),
        color: joi.string().max(50).required(),
        owner_id: joi.number().min(1).integer().positive().required(),
       
    }
)

const createCar = async (req, res, next) => {
    try {
        const { error, value } =  schemaBody.validate(req.body);
       
        if (error) {

            
            res.json({ Error: error.details[0].message})
            console.log(error);
            

        }
        else {
            next()
            console.log('Car successfully created');
            
        }

    } catch (error) {
         next(error);
    }
       


   
};

const schemaId = joi.object(
    {
        id: joi.number().min(1).integer().positive().required(),
    }
);
const getCar = async (req, res, next) => {
    try {
        const { error, value } =  schemaId.validate(req.params);
        if (error) {
            res.json({Error:error.details[0].message});
        }
        else {
            next();
        }
    } 
    catch (error) {
        next(error);
    }
};

const updateCar = async (req, res, next) => {
    try {
        const { errorId, valueId } =  schemaId.validate(req.params);
        const { error, value } =  schemaBody.validate(req.body);
        if (errorId) {
            res.json(errorId);
        }
        else if (error) {
            res.json({Error:error.details[0].message});
        }
        else {
            next();
        }

    }
    catch (error) {
         next(error);
        

    }
};

module.exports = {
    createCar,
    getCar,
    updateCar,
}
